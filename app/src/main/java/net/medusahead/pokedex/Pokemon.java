package net.medusahead.pokedex;

import android.graphics.Picture;

import com.caverock.androidsvg.SVG;

/**
 * Created by c on 4/6/16.
 */
public class Pokemon {
    /* TODO name array for things like (East) (West) etc. */
    private String english_name;
    private String japanese_name;

    /* Pokedex nr.
       ndex National Pokedex
       kdex Kanto
       jdex Johto
       hdex Hoenn
       sdex Sinnoh
       udex Unova
       kadex Kalos
     */
    private int ndex, kdex, jdex, hdex, sdex, udex, kadex;

    /* XXX Category class? */
    private String category;

    /* XXX Do I want this here? */
    private SVG svg;

    public Pokemon(String name, int ndex) {
    }

    public int getNdexNumber() {
        return ndex;
    }

    public String getEnglishName() {
        return english_name;
    }

    public String getJapaneseName() {
        return japanese_name;
    }

    public String getName() {
        return getEnglishName();
    }

    public Picture getImage() {
        return new Picture();
    }

    public String toString() {
        return "#" + getNdexNumber() + " - " + getName();
    }
}