package net.medusahead.pokedex;

/**
 * Created by c on 4/6/16.
 * https://stackoverflow.com/questions/41107/how-to-generate-a-random-alpha-numeric-string
 */
import java.math.BigInteger;
import java.security.SecureRandom;

public final class SessionIdentifierGenerator
{
    private SecureRandom random = new SecureRandom ();
    private int SESSION_ID_BITS = 256;
    private int SESSION_BASE = 16;

    public String nextSessionId ()
    {
        return new BigInteger (SESSION_ID_BITS,
                               random).toString (SESSION_BASE);
    }

    public String toString ()
    {
        return nextSessionId ();
    }

    public void setSessionBits (int bits)
    {
        SESSION_ID_BITS = bits;
    }

    public void setSessionBase (int base)
    {
        SESSION_BASE = base;
    }
}
