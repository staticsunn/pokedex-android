package net.medusahead.pokedex;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.caverock.androidsvg.SVG;
import com.caverock.androidsvg.SVGParseException;

import java.io.IOException;

/**
 * Created by c on 4/6/16.
 */
public class MainActivity extends Activity {

    private void renderSvg(AssetDatabase ad, ImageView im) {
        String filename = ad.getFilename();
        Log.i("MainActivity", filename);
        try {
            SVG svg = SVG.getFromAsset(getApplicationContext().getAssets(), filename);
            assert svg != null;
            Drawable drawable = new PictureDrawable(svg.renderToPicture());
            im.setImageDrawable(drawable);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SVGParseException e) {
            e.printStackTrace();
        }
    }

    private void addItemsToSpinner(AssetDatabase ad, Spinner sp) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, ad.getSpinnerData());
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp.setAdapter(dataAdapter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final AssetDatabase ad = new AssetDatabase(getAssets(), getResources());
        final Button previous_button = (Button) findViewById(R.id.activity_main_button_previous);
        final Button next_button = (Button) findViewById(R.id.activity_main_button_next);
        final ImageView imageView = (ImageView) findViewById(R.id.activity_main_imageview);
        final Spinner spinner = (Spinner) findViewById(R.id.activity_main_spinner);

        previous_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.prevAsset();
                spinner.setSelection(ad.getAssetNr());
                renderSvg(ad, imageView);
            }
        });

        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ad.nextAsset();
                spinner.setSelection(ad.getAssetNr());
                renderSvg(ad, imageView);
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ad.setAssetNr(position);
                renderSvg(ad, imageView);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        addItemsToSpinner(ad, spinner);
        renderSvg(ad, imageView);
    }
}