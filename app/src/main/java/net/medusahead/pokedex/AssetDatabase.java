package net.medusahead.pokedex;

import android.content.res.AssetManager;
import android.content.res.Resources;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

/**
 * Created by c on 4/6/16.
 */
public class AssetDatabase {
    /* assets/dream-world */
    public static final String ASSET_DREAM_WORLD = "dream-world";

    /* TODO add more assets from other games, cries, etc. */
    private static String ASSET_SELECTED = ASSET_DREAM_WORLD;

    /* Taken from http://bulbapedia.bulbagarden.net/wiki/Rdex */
    public static final String ASSET_NDEX = "ndex-names.txt";

    /* Taken from http://bulbapedia.bulbagarden.net/wiki/Rdex
       In the future I can forsee myself basically ripping the data
       from this site.
       TODO check if they have data dumps available.
    */
    public static final String ASSET_RDEX = "rdex-names.txt";

    /* TODO do away with these, they are just here because of the code I wrote in MainActivity */
    private int mAssetNr = 0;
    private String mFilename = "dream-world/1.svg";

    /* Reference to current Assets */
    private ArrayList<String> mAsset;

    private ArrayList<String> dreamWorldAssets;
    private ArrayList<String> rdexAssets;
    private ArrayList<String> ndexAssets;

    private AssetManager assetManager;
    private Resources resources;

    public AssetDatabase(AssetManager assetManager, Resources resources) {
        this.assetManager = assetManager;
        this.resources = resources;
        try {
            scanAssets();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void scanAssets() throws IOException {
        if (dreamWorldAssets == null) {
            dreamWorldAssets = new ArrayList<String>(loadElements(ASSET_DREAM_WORLD));
            Collections.sort(dreamWorldAssets, new AlphanumComparator());
        }
        if (rdexAssets == null) {
            rdexAssets = new ArrayList<String>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(assetManager.open(ASSET_RDEX)));
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                rdexAssets.add(mLine);
            }
            // XXX Do not sort! The file is in numeric order.
            // Scraped from here: http://bulbapedia.bulbagarden.net/wiki/Ndex
            // Collections.sort(nameAssets, new AlphanumComparator());
        }
        if (ndexAssets == null) {
            ndexAssets = new ArrayList<String>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(assetManager.open(ASSET_NDEX)));
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                ndexAssets.add(mLine);
            }
        }
        if (mAsset == null) {
            mAsset = dreamWorldAssets;
        }
    }

    private HashSet<String> loadElements(String path) throws IOException {
        HashSet<String> e = new HashSet<String>();
        String[] files = assetManager.list(path);
        for (String file : files) {
            Log.i("AssetDatabase", file);
            e.add(file);
        }
        return e;
    }

    public String getFilename () {
        return mFilename;
    }

    public void setFilename (String filename) {
        mFilename = filename;
    }

    public String getNameAsset (int nr) {
        return mAsset.get(nr);
    }

    public ArrayList<String> getSpinnerData() {
        ArrayList<String> a = new ArrayList<String>();
        int n = 0;
        for (String name : ndexAssets) {
            a.add("#" + (n + 1) + " - " + getNameAsset(n));
            ++n;
        }

        return a;
    }

    public void prevAsset() {
        updateAssetNr(--mAssetNr);
    }

    public void nextAsset() {
        updateAssetNr(++mAssetNr);
    }

    public int getAssetNr() {
        return mAssetNr;
    }

    public void updateAssetNr(int nr) {
        setAssetNr(getAssetNr());
    }

    public void setAssetNr(int nr) {
        setAssetNr(nr, false);
    }

    public void setAssetNr(int nr, boolean wrap) {
        if (wrap) {
            if (nr > mAsset.size())
                nr = 0;
            if (nr < 0)
                nr = mAsset.size() - 1;
        }
        else {
            if (nr > mAsset.size())
                nr = mAsset.size() - 1;
            if (nr < 0)
                nr = 0;
        }

        mAssetNr = nr;
        mFilename = new File(ASSET_SELECTED, mAsset.get(nr)).toString();
        Log.i("AssetDatabase", mFilename);
    }

    public String getNameAsset () {
        int n = getAssetNr();
        String s = getNameAsset(n);
        Log.i("AssetDatabase", s);
        return s;
    }
}
