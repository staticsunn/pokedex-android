package net.medusahead.pokedex;

import android.content.res.AssetManager;
import android.content.res.Resources;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by c on 4/7/16.
 */
public class Pokedex {
    private static AssetManager assetManager;
    private static Resources resources;
    private static AssetDatabase ad;
    private static ArrayList<Pokemon> pokemons;

    Pokedex(AssetManager assetManager, Resources resources) {
        this.assetManager = assetManager;
        this.resources = resources;

        ad = new AssetDatabase(assetManager, resources);

        try {
            buildPokedex();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void buildPokedex() throws IOException {
        if (pokemons == null) {
            pokemons = new ArrayList<Pokemon>();
        }
    }
}
