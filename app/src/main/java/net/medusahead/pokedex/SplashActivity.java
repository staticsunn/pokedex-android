package net.medusahead.pokedex;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

/**
 * Created by c on 4/6/16.
 */
public class SplashActivity extends Activity
{
    ProgressBar progressBar;

    /** Called when the activity is first created. */
    @Override
    public void onCreate (Bundle savedInstanceState)
    {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_splash);

        progressBar = (ProgressBar) findViewById (R.id.activity_splash_progressbar);
        assert progressBar != null;
        progressBar.setVisibility (View.VISIBLE);

        new Thread (new Runnable () {
            public void run () {
                try {
                    Thread.sleep (1000);
                    startActivity (new Intent ("net.medusahead.pokedex.CLEARSCREEN"));
                }
                catch (InterruptedException e) {
                    e.printStackTrace ();
                }
                finally {
                    finish ();
                }
            }
        }).start ();
    }
}
